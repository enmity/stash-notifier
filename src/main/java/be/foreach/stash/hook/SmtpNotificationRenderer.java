package be.foreach.stash.hook;

import com.atlassian.stash.content.*;
import com.atlassian.stash.history.HistoryService;
import com.atlassian.stash.mail.MailMessage;
import com.atlassian.stash.mail.MailService;
import com.atlassian.stash.nav.NavBuilder;
import com.atlassian.stash.repository.Repository;
import com.atlassian.stash.scm.git.GitCommand;
import com.atlassian.stash.scm.git.GitCommandBuilderFactory;
import com.atlassian.stash.util.Page;
import com.atlassian.stash.util.PageRequestImpl;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class SmtpNotificationRenderer {

    private static final Logger LOG = LoggerFactory.getLogger(SmtpNotificationRenderer.class);
    private static final String BRANCH_SEP = " / ";
    private final MailService mailService;
    private final HistoryService historyService;
    private final NavBuilder navBuilder;
    private final GitCommandBuilderFactory gitCommandBuilderFactory;

    public SmtpNotificationRenderer( final MailService mailService, final HistoryService historyService, final NavBuilder navBuilder, final GitCommandBuilderFactory gitCommandBuilderFactory ) {
        this.mailService = mailService;
        this.historyService = historyService;
        this.navBuilder = navBuilder;
        this.gitCommandBuilderFactory = gitCommandBuilderFactory;
    }

    public void sendMail( List<String> emailAddresses, String from, String subject, String body ) {
        for( String emailAddress : emailAddresses ) {
            MailMessage mailMessage = new MailMessage.Builder().to( emailAddress ).from(from).subject(subject).text(body).header("Content-type", "text/html; charset=UTF-8").build();
            mailService.submit( mailMessage );
        }

    }

    public String getHtml(final Repository repository, final Page<Changeset> page, boolean newBranch ) {
        boolean hasCommitsOnThisBranch = false;

        StringBuilder sb = new StringBuilder();

        sb.append( "<html><!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.1//EN\"\n" +
                "\"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">\n" +
                "<head>\n" +
                "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"></head>\n" +
                "<html xmlns=\"http://www.w3.org/1999/xhtml\">\n" +
                "<head><style type=\"text/css\"><!--\n" +
                "#msg dl { border: 1px #006 solid; background: #369; padding: 6px; color: #fff; }\n" +
                "#msg dt { float: left; width: 6em; font-weight: bold; }\n" +
                "#msg dt:after { content:':';}\n" +
                "#msg dl, #msg dt, #msg ul, #msg li, #header, #footer { font-family: verdana,arial,helvetica,sans-serif; font-size: 10pt;  }\n" +
                //"#msg dl a { font-weight: bold}\n" +
                "#msg dl a:link    { color:#fc3; }\n" +
                "#msg dl a:active  { color:#ff0; }\n" +
                "#msg dl a:visited { color:#cc6; }\n" +
                "h3 { font-family: verdana,arial,helvetica,sans-serif; font-size: 10pt; font-weight: bold; }\n" +
                "#msg pre { overflow: auto; background: #ffc; border: 1px #fc0 solid; padding: 6px; }\n" +
                "#msg ul, pre { overflow: auto; }\n" +
                "#header, #footer { color: #fff; background: #636; border: 1px #300 solid; padding: 6px; }\n" +
                "#patch { width: 100%; }\n" +
                "#patch h4 {font-family: verdana,arial,helvetica,sans-serif;font-size:10pt;padding:8px;background:#369;color:#fff;margin:0;}\n" +
                "#patch .propset h4, #patch .binary h4 {margin:0;}\n" +
                "#patch pre {padding:0;line-height:1.2em;margin:0;}\n" +
                "#patch .diff {width:100%;background:#eee;padding: 0 0 10px 0;overflow:auto;}\n" +
                "#patch .propset .diff, #patch .binary .diff  {padding:10px 0;}\n" +
                "#patch span {display:block;padding:0 10px;}\n" +
                "#patch .modfile, #patch .addfile, #patch .delfile, #patch .propset, #patch .binary, #patch .copfile {border:1px solid #ccc;margin:10px 0;}\n" +
                "#patch .lines, .info {color:#888;background:#fff;}\n" +
                "--></style>\n" +
                "<title>TITLE</title>\n" +
                "</head>\n" +
                "<body style=\"font-family: verdana,arial,helvetica,sans-serif; font-size: 10pt;\">\n" );

        boolean isSkippingCommits = false;
        for( Changeset c : page.getValues() ) {
            GitStringOutputHandler stringOutputHandler = new GitStringOutputHandler();
            GitCommand<String> gitCommand = gitCommandBuilderFactory.builder( repository ).command( "branch" ).argument( "-a" ).argument( "--contains" ).argument( c.getId() ).build(stringOutputHandler);
            String output = gitCommand.call();

            String branches = null;
            int numberOfBranches = 1;
            if( output != null ) {
                branches = StringUtils.stripEnd( output.replaceAll("(\\t|\\r?\\n)+", BRANCH_SEP), BRANCH_SEP );
                String[] numOfBranches = branches.split( BRANCH_SEP );
                numberOfBranches = numOfBranches.length;
                if( numberOfBranches == 1 ) {
                    hasCommitsOnThisBranch = true;
                    LOG.debug( "This commit is only on this branch" );
                }
            }

            if( ( newBranch && numberOfBranches == 1 ) || !newBranch ) {
                sb.append( "<div id=\"msg\">" );
                isSkippingCommits = false;

                String parentsSb = getParents(repository, c);
                writeCommitData(repository, page, sb, c, parentsSb, branches);
            } else {
                if( !isSkippingCommits ) {
                    sb.append( "Skipping commits from other branches: ");
                    isSkippingCommits = true;
                }
                sb.append( c.getDisplayId() ).append( " " );
            }
            if( !isSkippingCommits ) {
                sb.append( "</div>" );
            }
        }
        sb.append( "</body></html>" );

        if( newBranch ) {
            if( hasCommitsOnThisBranch ) {
                return sb.toString();
            } else {
                return "No interesting commits on this new branch yet";
            }

        } else {
            return sb.toString();
        }
    }

    private String getParents( final Repository repository, final Changeset changeSet) {
        StringBuilder parentsSb = new StringBuilder();
        Collection<MinimalChangeset> parents = changeSet.getParents();
        if( parents != null ) {
            Iterator<MinimalChangeset> parentIt = parents.iterator();
            if( parents.size() > 1 ) {
                parentsSb.append( " - " ).append( "parents" ).append( ":" );
                while( parentIt.hasNext() ) {
                    MinimalChangeset minimalChangeset = parentIt.next();
                    String url = navBuilder.repo( repository ).changeset( minimalChangeset.getId() ).buildConfigured();
                    parentsSb.append( "<a href=\"" ).append( url ).append( "\">" ).append( minimalChangeset.getDisplayId() ).append( "</a>" );
                    if( parentIt.hasNext() ) {
                        parentsSb.append( "+" );
                    }
                }
            }
        }
        return parentsSb.toString();
    }

    private void writeCommitData( final Repository repository, final Page<Changeset> page, StringBuilder sb, final Changeset changeset, String parents, String branches) {
        sb.append( "<dl>" );
        String url = navBuilder.repo( repository ).changeset( changeset.getId() ).buildConfigured();
        String commit = "<a href=\"" + url + "\">" + changeset.getDisplayId() + "</a>";
        if( parents.length() > 0 ) {
            commit += parents;
        }

        addMeta(sb, "Commit", commit );
        if( branches != null ) {
            addMeta(sb, "Contained in branches",  branches );
        }

        addMeta(sb, "Author", changeset.getAuthor().getName() + " - " +  "<a href=\"mailto:" + changeset.getAuthor().getEmailAddress() + "\">" + changeset.getAuthor().getEmailAddress() + "</a>" );
        addMeta(sb, "Date", changeset.getAuthorTimestamp().toString());
        sb.append("</dl>");

        sb.append( "<h3>Log message</h3>" );
        String escapeHTML = StringEscapeUtils.escapeHtml( changeset.getMessage() );
        sb.append( "<pre style=\"overflow: auto; background: #ffc; border: 1px #fc0 solid; padding: 6px;\">").append( escapeHTML ).append( "</pre>" );
        sb.append( "</div>" );

        sb.append( "<div id=\"patch\">" );
        sb.append( "<h3>Modified paths</h3>" );

        witeCommitChanges(repository, page, sb, changeset, url);
        //TODO: implement inline diffs, configurable by a setting?
        //sb.append( "<h3>Diff</h3>" );

    }

    private void witeCommitChanges( final Repository repository, final Page<Changeset> page, StringBuilder sb, final Changeset changeSet, String url) {
        int maxChangesPerCommit = PushEmailNotifier.MAX_PAGE_REQUEST;
        if( page.getSize() > 100 ) {
            // If this is a rather large merge, limit the number of changes to 11, so we can show a ... after the 10th change item
            maxChangesPerCommit = 11;
        }
        //TODO: make max changes configurable ?
        DetailedChangesetsRequest.Builder detailedChangesetsRequest = new DetailedChangesetsRequest.Builder( repository ).ignoreMissing(true).maxChangesPerCommit( maxChangesPerCommit );
        detailedChangesetsRequest.changesetId( changeSet.getId() );

        // Make number of change sets configurable
        Page<DetailedChangeset> changeSets = historyService.getDetailedChangesets(detailedChangesetsRequest.build(), new PageRequestImpl(0, PushEmailNotifier.MAX_PAGE_REQUEST) );
        for( DetailedChangeset detailedChangeset : changeSets.getValues() ) {
            Page<? extends Change> detailedChanges = detailedChangeset.getChanges();
            if( detailedChanges != null ) {
                sb.append( "<ul>" );
                int i = 1;
                for( Change change : detailedChanges.getValues() ) {
                    if( i > maxChangesPerCommit - 1 ) {
                        break;
                    }
                    sb.append( "<li><a href=\"" ).append( url ).append( "#" ).append( change.getPath() ).append( "\">")
                            .append(change.getPath()).append( " (" ).append( change.getType() ).append( ")" )
                            .append("</a></li>");
                    i++;
                }
                if( detailedChanges.getSize() > maxChangesPerCommit - 1 ) {
                    // More than 10 changes
                    sb.append( "<li><a href=\"" ).append( url ).append( "\">").append( "..." ).append("</a></li>");
                }
                sb.append("</ul>");
            }
        }
    }

    private void addMeta( StringBuilder sb, String header , String value ) {
        sb.append( "<dt>" ).append( header ).append( "</dt><dd>" ).append( value ).append( "</dd>" );
    }
}
