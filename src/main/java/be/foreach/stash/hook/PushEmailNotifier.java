package be.foreach.stash.hook;

import com.atlassian.stash.content.Changeset;
import com.atlassian.stash.content.ChangesetsBetweenRequest;
import com.atlassian.stash.history.HistoryService;
import com.atlassian.stash.hook.repository.AsyncPostReceiveRepositoryHook;
import com.atlassian.stash.hook.repository.RepositoryHookContext;
import com.atlassian.stash.repository.RefChange;
import com.atlassian.stash.repository.Repository;
import com.atlassian.stash.scm.git.GitCommand;
import com.atlassian.stash.scm.git.GitCommandBuilderFactory;
import com.atlassian.stash.server.ApplicationPropertiesService;
import com.atlassian.stash.setting.RepositorySettingsValidator;
import com.atlassian.stash.setting.Settings;
import com.atlassian.stash.setting.SettingsValidationErrors;
import com.atlassian.stash.user.StashAuthenticationContext;
import com.atlassian.stash.user.StashUser;
import com.atlassian.stash.util.Page;
import com.atlassian.stash.util.PageRequestImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class PushEmailNotifier implements AsyncPostReceiveRepositoryHook, RepositorySettingsValidator {
    private static final String BRANCH_CREATION_OR_DELETION = "0000000000000000000000000000000000000000";
    private static final Logger LOG = LoggerFactory.getLogger( PushEmailNotifier.class );

    private final HistoryService historyService;
    private final SmtpNotificationRenderer smtpNotificationRenderer;
    private final StashAuthenticationContext stashAuthenticationContext;
    private final ApplicationPropertiesService applicationPropertiesService;
    private final GitCommandBuilderFactory gitCommandBuilderFactory;

    public static final int MAX_PAGE_REQUEST = 1000000;

    public PushEmailNotifier( final HistoryService historyService, final SmtpNotificationRenderer smtpNotificationRenderer, final StashAuthenticationContext stashAuthenticationContext,
                              final ApplicationPropertiesService applicationPropertiesService, final GitCommandBuilderFactory gitCommandBuilderFactory ) {
        this.historyService = historyService;
        this.smtpNotificationRenderer = smtpNotificationRenderer;
        this.stashAuthenticationContext = stashAuthenticationContext;
        this.applicationPropertiesService = applicationPropertiesService;
        this.gitCommandBuilderFactory = gitCommandBuilderFactory;
    }

    private String getCurrentUser() {
        StashUser stashUser = stashAuthenticationContext.getCurrentUser();
        if (stashUser != null) {
            return stashUser.getDisplayName() + " - " + stashUser.getEmailAddress();
        } else {
            return "ANONYMOUS";
        }
    }

    public void postReceive(RepositoryHookContext context, Collection<RefChange> refChanges) {
        LOG.debug( "refchanges size:" + refChanges.size() );
        String mailTo = context.getSettings().getString("mailToAddress");

        if (mailTo != null) {
            List<String> emailAddresses = new ArrayList<String>();
            if( mailTo.indexOf( ';' ) >= 0 ) {
                String[] splitEmails = mailTo.split( ";" );
                for (String splitEmail : splitEmails) {
                    emailAddresses.add(splitEmail.trim());
                }
            } else {
                emailAddresses.add( mailTo.trim() );
            }
            Repository repository = context.getRepository();
            String fromName = getFromName(applicationPropertiesService, repository);

            for (RefChange refChange : refChanges) {
                LOG.info("Checking ref:{}, type:{}, from:{}, to:{}", refChange.getRefId(), refChange.getType(), refChange.getFromHash(), refChange.getToHash() );
                if (BRANCH_CREATION_OR_DELETION.equals(refChange.getToHash())) {
                    String subject = getSubjectPrefix( refChange, getShortHash( refChange.getFromHash() ) + ".." + getShortHash( refChange.getToHash() ) ) + "Branch deleted";
                    smtpNotificationRenderer.sendMail( emailAddresses, fromName, subject, "Branch deleted by " + getCurrentUser() );
                } else {
                    ChangesetsBetweenRequest changesetsBetweenRequest = new ChangesetsBetweenRequest.Builder(repository).exclude(refChange.getFromHash(), new String[0]).include(refChange.getToHash(), new String[0]).build();
                    int maxChangeSets = MAX_PAGE_REQUEST;
                    boolean newBranch = false;
                    if( BRANCH_CREATION_OR_DELETION.equals( refChange.getFromHash() ) ) {
                        // When creating and pushing a new branch, don't fetch all changes, limit it to somewhat 100
                        maxChangeSets = 100;
                        newBranch = true;
                    }
                    Page<Changeset> page = historyService.getChangesetsBetween(changesetsBetweenRequest, new PageRequestImpl( 0, maxChangeSets ));
                    String body = smtpNotificationRenderer.getHtml(repository, page, newBranch );

                    String changeSet = getShortHash(refChange.getToHash());
                    String commitMessage;
                    if (page.getSize() > 1) {
                        changeSet = getShortHash(refChange.getFromHash()) + ".." + changeSet;
                        if( BRANCH_CREATION_OR_DELETION.equals( refChange.getFromHash() ) ) {
                            commitMessage = "Branch created";
                            body = "Branch created by " + getCurrentUser() + "<br/><br/>" + body;
                        } else {
                            int numberOfCommits = getRealNumberOfCommits( repository, changeSet );
                            commitMessage = "(" + numberOfCommits + " commits)";
                        }
                    } else {
                        commitMessage = page.getValues().iterator().next().getMessage();
                        commitMessage = commitMessage.replaceAll("(\\t|\\r?\\n)+", " ");
                    }

                    smtpNotificationRenderer.sendMail( emailAddresses, fromName, getSubjectPrefix(refChange, changeSet) + commitMessage, body);
                }
            }
        }
    }

    private int getRealNumberOfCommits(Repository repository, String changeSet ) {
        GitStringOutputHandler stringOutputHandler = new GitStringOutputHandler();
        GitCommand<String> gitCommand = gitCommandBuilderFactory.builder( repository ).command( "rev-list" ).argument( changeSet ).argument( "--count" ).build(stringOutputHandler);
        String output = gitCommand.call();
        if( output != null) {
            return Integer.parseInt( output.replaceAll("(\\t|\\r?\\n)+", "") );
        }
        return 0;
    }

    private String getSubjectPrefix(RefChange refChange, String changeSet) {
        return "[" + changeSet + "@" + refChange.getRefId() + "] ";
    }

    private String getFromName(ApplicationPropertiesService applicationPropertiesService, Repository repository) {
        String from = applicationPropertiesService.getServerEmailAddress();
        if( from == null ) {
            return "repo_" + repository.getId() + "@NOT_CONFIGURED.COM";
        } else {
            String domain = from.split("@")[1];
            return "repo_" + repository.getId() + "@" + domain;
        }
    }

    private String getShortHash(String refId) {
        return refId.substring(0, 7);
    }

    @Override
    public void validate(Settings settings, SettingsValidationErrors errors, Repository repository) {
        if (settings.getString("mailToAddress", "").isEmpty()) {
            errors.addFieldError("mailToAddress", "Please provide an email address");
        }
    }
}